/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        dark: '#222935',
        error: '#da2525',
        grey: '#667081',
        green: '#26C17E',
        lightGrey: '#EEEEEE',
        lightGreyForTime: '#667081',
        lightGreyForBorder: '#DDDDDD',
        darkGrey: '#000614'
      }
    }
  },
  plugins: []
};

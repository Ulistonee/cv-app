import React from 'react';

const feedbacks = [
  {
    text: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ',
    avatar: 'assets/images/Ellipse 1.png',
    name: 'Martin Friman Programmer',
    location: 'somesite.com',
    id: '1'
  },
  {
    text: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ',
    avatar: 'assets/images/Ellipse 1.png',
    name: 'Martin Friman Programmer',
    location: 'somesite.com',
    id: '2'
  }
];

export const Feedback = () => {
  return (
    <section>
      <ul>
        {feedbacks.map((feedback) => {
          return (
            <li key={feedback.id}>
              <p className="bg-lightGrey p-5">{feedback.text}</p>
              <div className="flex mt-5 mb-5 items-center	">
                <img
                  src={feedback.avatar}
                  alt="photo of a user who left feedback"
                  className="pr-5"
                />
                <p className={'inline'}>
                  <span>{feedback.name + ', '}</span>
                  <span className="text-green">{feedback.location}</span>
                </p>
              </div>
            </li>
          );
        })}
      </ul>
    </section>
  );
};

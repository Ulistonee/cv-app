import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSync } from '@fortawesome/free-solid-svg-icons';

export const Loader = () => {
  return (
    <div className={'w-full min-h-[200px] flex justify-center items-center'}>
      <FontAwesomeIcon className={'fas fa-sync fa-spin text-green'} icon={faSync} />
    </div>
  );
};

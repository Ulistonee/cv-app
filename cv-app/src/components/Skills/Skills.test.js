import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { Skills } from './Skills';
import { Provider } from 'react-redux';
import store from '../../app/store';

test('Skills components renders without errors', () => {
  render(
    <Provider store={store}>
      <Skills />
    </Provider>
  );
});

test('On Open edit click Skills Form toggles', () => {
  const skills = render(
    <Provider store={store}>
      <Skills />
    </Provider>
  );
  const openEdit = skills.getByTestId('open edit');
  fireEvent.click(openEdit);
  const addSkillForm = skills.getByTestId('submit-form');
  expect(addSkillForm).toBeInTheDocument();
});

test('Open edit is clickable', () => {
  let counter = 0;
  const skills = render(
    <Provider store={store}>
      <Skills />
    </Provider>
  );
  const addSkillForm = skills.getByTestId('submit-form');
  if (fireEvent.click(addSkillForm)) {
    counter++;
  }
  console.log(fireEvent.click(addSkillForm));
  expect(counter).toBe(1);
});

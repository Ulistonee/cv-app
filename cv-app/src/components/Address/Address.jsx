import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faTwitter, faFacebookF, faSkype } from '@fortawesome/free-brands-svg-icons';

const contacts = [
  {
    type: 'phone',
    icon: faPhone,
    contact: '500 342 242',
    link: ''
  },
  {
    type: 'email',
    icon: faEnvelope,
    contact: 'office@kamsolutions.pl',
    link: ''
  },
  {
    type: 'twitter',
    icon: faTwitter,
    contact: 'Twitter',
    link: 'https://twitter.com/wordpress'
  },
  {
    type: 'facebook',
    icon: faFacebookF,
    contact: 'Facebook',
    link: 'https://www.facebook.com/facebook'
  },
  {
    type: 'skype',
    icon: faSkype,
    contact: 'Skype',
    link: 'kamsolutions.pl'
  }
];

export const Address = () => {
  return (
    <section>
      <ul className={'flex flex-col gap-4'}>
        {contacts.map((contact, index) => {
          return (
            <li key={contact.contact} className="flex items-center gap-4">
              <div className="w-[35px] h-[48px] flex items-center justify-center">
                <FontAwesomeIcon icon={contact.icon} className="text-green fa-xl" />
              </div>
              <div>
                <p className="font-bold text-base">{contact.contact}</p>
                {contact.link ? <p className="text-lightGreyForTime">{contact.link}</p> : ' '}
              </div>
            </li>
          );
        })}
      </ul>
    </section>
  );
};

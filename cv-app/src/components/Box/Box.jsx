import React from 'react';

export const Box = ({ title, children }) => {
  return (
    <section className="">
      <h1 className="font-bold text-green text-2xl pt-10 pb-10 leading-7">{title}</h1>
      {children}
    </section>
  );
};

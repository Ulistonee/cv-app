import React from 'react';

export const Photobox = ({ avatar, name }) => {
  return (
    <div className="flex flex-col items-center justify-center pt-7">
      <img src={avatar} alt="user avatar" className="pb-2" />
      <p className="text-white text-base font-bold">{name}</p>
    </div>
  );
};

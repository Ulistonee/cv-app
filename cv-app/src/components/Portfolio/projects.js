export const projects = [
  {
    image: 'assets/images/card_1.png',
    title: 'Some text',
    id: '1',
    description:
      'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis',
    link: 'View resource',
    type: 'Code'
  },
  {
    image: 'assets/images/card_3.png',
    title: 'Some text',
    id: '2',
    description:
      'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis',
    link: 'View resource',
    type: 'UI'
  },
  {
    image: 'assets/images/card_1.png',
    title: 'Some text',
    id: '3',
    description:
      'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis',
    link: 'View resource',
    type: 'Code'
  }
];

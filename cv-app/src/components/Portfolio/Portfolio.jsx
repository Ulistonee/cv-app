import React, { useState, useEffect, useRef } from 'react';
import { projects } from './projects';
import Isotope from 'isotope-layout';
import { clsx } from 'clsx';

export const Portfolio = () => {
  const [selectedType, setSelectedType] = React.useState('*');

  return (
    <>
      <ul className="flex pb-5 space-x-2">
        <li
          className={clsx('hover:text-green cursor-pointer', selectedType === '*' && 'text-green')}
          onClick={() => setSelectedType('*')}
        >
          All /
        </li>
        <li
          className={clsx(
            'hover:text-green cursor-pointer',
            selectedType === 'Code' && 'text-green'
          )}
          onClick={() => setSelectedType('Code')}
        >
          Code /
        </li>
        <li
          className={clsx('hover:text-green cursor-pointer', selectedType === 'UI' && 'text-green')}
          onClick={() => setSelectedType('UI')}
        >
          UI
        </li>
      </ul>
      <ul className="filter-container flex flex-col md:flex-row gap-5 max-w-full overflow-x-auto">
        {projects
          .filter((project) => project.type === selectedType || selectedType === '*')
          .map((project) => {
            return (
              <li
                className={clsx('group relative overflow-hidden h-[190px] w-[308px] shrink-0')}
                key={project.id}
              >
                <img src={project.image} alt="preview of a project" className="h-full w-full" />
                <article className="border border-1 border-lightGreyForBorder p-3 absolute top-0 left-0 w-full h-full bg-white translate-y-full group-hover:translate-y-0 transition-all duration-300 ease-in-out">
                  <h2 className="text-green font-bold text-base">{project.title}</h2>
                  <p>{project.description}</p>
                  <a href={'#'} className="text-green underline">
                    {project.link}
                  </a>
                </article>
              </li>
            );
          })}
      </ul>
    </>
  );
};

import React from 'react';
import clsx from 'clsx';

const typeClasses = {
  primary: 'bg-green text-white',
  secondary: 'bg-dark text-white'
};

export const Button = ({ variant = 'primary', children, className, ...args }) => {
  return (
    <button
      className={clsx(
        'px-5 py-2 rounded disabled:bg-gray-600 disabled:text-dark',
        typeClasses[variant],
        className
      )}
      {...args}
    >
      {children}
    </button>
  );
};

import { clsx } from 'clsx';
import React from 'react';
import { Photobox } from '../Photobox';
import { Navigation } from '../Navigation';

export const Panel = ({ open }) => {
  return (
    <div
      className={clsx(
        'transition-all duration-300 ease-in overflow-hidden bg-dark min-h-full',
        open ? 'w-[250px]' : 'w-0'
      )}
    >
      <Photobox avatar="assets/images/User avatar.png" name="John Doe" />
      <Navigation />
    </div>
  );
};

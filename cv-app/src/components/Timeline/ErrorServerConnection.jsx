import React from 'react';

export const ErrorServerConnection = () => {
  return (
    <div className={'w-full min-h-[200px] flex justify-center items-center text-red-500 font-bold'}>
      Something went wrong; please review your server connection!
    </div>
  );
};

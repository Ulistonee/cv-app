import React from 'react';
import { render } from '@testing-library/react';
import { Timeline } from './Timeline';
import store from '../../app/store';
import { Provider } from 'react-redux';

test('Timeline components renders without errors', () => {
  render(
    <Provider store={store}>
      <Timeline />
    </Provider>
  );
});

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchEducation, selectEducation } from '../../features/educaton/educationSlice';
import { Loader } from '../Loader';
import { ErrorServerConnection } from './ErrorServerConnection';

export const Timeline = () => {
  const { education, loading, error } = useSelector(selectEducation);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchEducation());
  }, []);

  return (
    <div className={'w-full'}>
      {loading && <Loader />}
      {error && <ErrorServerConnection />}
      {!loading && !error && education?.length > 0 && (
        <ul className={'space-y-3'}>
          {education.map((item) => {
            return (
              <li
                key={item.title}
                className="flex relative before:absolute before:top-12 before:left-4 before:h-2/3 before:w-[5px] before:bg-green"
              >
                <span className="font-bold pr-10 pt-3">{item.date}</span>
                <div className="bg-lightGrey p-4 relative">
                  <div className="absolute top-5 -left-1 w-[10px] h-[10px] rotate-45 z-10 bg-lightGrey"></div>
                  <h2 className="font-bold">{item.title}</h2>
                  <p>{item.description}</p>
                </div>
              </li>
            );
          })}
        </ul>
      )}
    </div>
  );
};

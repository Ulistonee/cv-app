import { render, screen } from '@testing-library/react';
import App from './App';
import React from 'react';

test('рендеринг компонента App', () => {
  render(<App />);

  expect(screen.getByText('John Doe')).toBeInTheDocument();
  expect(screen.getByRole('button', { name: 'Know more' })).toBeInTheDocument();
});

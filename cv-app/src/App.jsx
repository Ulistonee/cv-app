import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Home } from './pages/Home';
import { CV } from './pages/CV';
import './services/server';
import store from './app/store';
import { Provider } from 'react-redux';

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/CV" element={<CV />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;

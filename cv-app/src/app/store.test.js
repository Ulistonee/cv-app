import { configureStore } from '@reduxjs/toolkit';
import educationReducer from '../features/educaton/educationSlice';
import skillsReducer from '../features/skills/skillsSlice';

test('store initialization', () => {
  const store = configureStore({
    reducer: {
      education: educationReducer,
      skills: skillsReducer
    }
  });

  const initialState = store.getState();

  expect(initialState).toEqual({
    education: {
      education: [],
      loading: false,
      error: ''
    },
    skills: {
      skills: [],
      loading: false,
      error: ''
    }
  });
});

import { createServer, Model, RestSerializer } from 'miragejs';
const dbData = localStorage.getItem('db');

class Skills extends Model {}
Skills.displayName = {
  name: '',
  range: 10
};
export let server = createServer({
  models: {
    skills: Skills
  },

  serializers: {
    application: RestSerializer
  }
});
const defaultDB = {
  skills: [],
  education: [
    {
      date: '2001',
      title: 'Title 1',
      description:
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.'
    },
    {
      date: '2002',
      title: 'Title 2',
      description:
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.'
    },
    {
      date: '2012',
      title: 'Title 3',
      description:
        'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.'
    }
  ]
};
server.db.loadData(dbData ? JSON.parse(dbData) : defaultDB);

server.pretender.handledRequest = function (verb) {
  if (verb.toLowerCase() !== 'get' && verb.toLowerCase() !== 'head') {
    localStorage.setItem('db', JSON.stringify(server.db.dump()));
  }
};

server.get(
  '/api/education',
  (schema) => {
    return schema.db.education;
  },
  { timing: 1000 }
);

server.get(
  '/api/skills',
  (schema) => {
    return schema.skills.all();
  },
  { timing: 1000 }
);
server.post('/api/skills', (schema, request) => {
  let attrs = JSON.parse(request.requestBody);
  return schema.skills.create(attrs);
});

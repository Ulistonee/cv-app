import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

export const initialState = {
  skills: [],
  loading: false,
  error: ''
};

export const fetchSkills = createAsyncThunk('skills/fetch', async () => {
  return fetch('/api/skills', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then((res) => res.json())
    .then((data) => data.skills);
});

const skillsReducer = createSlice({
  name: 'skills',
  initialState,
  extraReducers: (builder) => {
    builder.addCase(fetchSkills.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchSkills.fulfilled, (state, action) => {
      state.loading = false;
      state.skills = action.payload;
    });
    builder.addCase(fetchSkills.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });
  }
});

export default skillsReducer.reducer;

export const selectSkills = (state) => state.skills;
export const { ordered, restocked } = skillsReducer.actions;

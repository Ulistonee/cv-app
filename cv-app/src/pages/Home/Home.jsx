import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from '../../components/Button';

export const Home = () => {
  return (
    <div
      className={
        'relative w-full h-screen text-white flex justify-center ' +
        "bg-[url('assets/images/image2.png')] bg-cover bg-center bg-no-repeat overflow-hidden pt-40 overflow-y-auto"
      }
    >
      <div className="absolute inset-0 bg-black opacity-50 w-full"></div>
      <div className={'space-y-4 flex flex-col items-center z-20 w-2/4 sm:w-1/4'}>
        <img
          src={'assets/images/User_avatar.png'}
          className={'h-32 w-32 rounded-full border border-white bg-gray-800'}
          alt={'User avatar'}
        ></img>
        <h1 className={'text-5xl font-bold text-center'}>John Doe</h1>
        <h2 className={'text-2xl font-semibold text-center'}>Programmer. Creative. Innovator</h2>
        <p className={'text-m text-center'}>
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
          dolor. Aenean massa. Cum sociis natoque
        </p>
        <Link to={'/cv'}>
          <Button variant={'secondary'} className={'z-10 m-3 hover:bg-green'}>
            Know more
          </Button>
        </Link>
      </div>
    </div>
  );
};

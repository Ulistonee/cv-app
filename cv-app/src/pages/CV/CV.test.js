import { fireEvent, act, render, screen } from '@testing-library/react';
import { CV } from './CV';
import React from 'react';
import { Provider } from 'react-redux';
import store from '../../app/store';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

test('CV component rendering', () => {
  render(
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/CV" element={<CV />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
});

test('On click the menu toggles', async () => {
  const cv = render(
    <Provider store={store}>
      <BrowserRouter>
        <CV />
      </BrowserRouter>
    </Provider>
  );
  // console.log('cv', cv);
  // screen.debug();
  const burgerButton = cv.getByTestId('burger');
  // fireEvent.click(burgerButton);
  const back = cv.getByText('Go back');
  expect(back).toBeInTheDocument();
});

// test('On click the menu toggles', async () => {
//   const cv = render(
//     <Provider store={store}>
//       <BrowserRouter>
//         <CV />
//       </BrowserRouter>
//     </Provider>
//   );
//   const burgerButton = cv.getByTestId('burger');
//   fireEvent.click(burgerButton);
//   console.log(cv.getByTestId('back'));
//   expect(cv.getByTestId('back')).toBeNull();
// });

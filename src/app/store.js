import { configureStore, createListenerMiddleware } from '@reduxjs/toolkit';
import educationReducer, {
  initialState as educationInitialState
} from '../features/educaton/educationSlice';
import skillsReducer, {
  fetchSkills,
  initialState as skillsInitialState
} from '../features/skills/skillsSlice';

// const listenerMiddleware = createListenerMiddleware();
// listenerMiddleware.startListening({
//   actionCreator: fetchSkills.fulfilled,
//   effect: (action, newStore) => {
//     const { skills } = newStore.getState();
//     localStorage.setItem('skills', JSON.stringify(skills.skills));
//   }
// });
//
// const getStateFromLocalStorage = () => {
//   const localStorageSkills = localStorage.getItem('skills');
//   if (localStorageSkills !== null) {
//     return {
//       education: educationInitialState,
//       skills: {
//         ...skillsInitialState,
//         skills: JSON.parse(localStorageSkills)
//       }
//     };
//   }
// };

const store = configureStore({
  reducer: {
    education: educationReducer,
    skills: skillsReducer
  }
  // preloadedState: getStateFromLocalStorage(),
  // middleware: (getDefaultMiddleware) => {
  //   return getDefaultMiddleware().prepend(listenerMiddleware.middleware);
  // }
});

export default store;

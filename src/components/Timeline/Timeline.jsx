import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchEducation, selectEducation } from '../../features/educaton/educationSlice';
import { Loader } from '../Loader';
import { ErrorServerConnection } from './ErrorServerConnection';

export const Timeline = () => {
  const { education, loading, error } = useSelector(selectEducation);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchEducation());
  }, []);

  return (
    <div className={'w-full relative'}>
      {loading && <Loader />}
      {error && <ErrorServerConnection />}
      {!loading && !error && education?.length > 0 && (
        <ul
          className={
            'space-y-3 before:absolute before:top-0 before:left-9 before:h-full before:w-[5px] before:bg-green sm:before:left-24'
          }
        >
          {education.map((item) => {
            return (
              <li key={item.title} className="flex items-start">
                <span className="shrink-0 bg-white z-10 font-bold pr-5 pt-3 w-16 sm:w-32 sm:py-2 text-right">
                  {item.date}
                </span>
                <div className="bg-lightGrey p-4 relative w-2/5">
                  <div className="absolute top-5 -left-1 w-[10px] h-[10px] rotate-45 z-10 bg-lightGrey"></div>
                  <h2 className="font-bold">{item.title}</h2>
                  <p className={'truncate'}>{item.description}</p>
                </div>
              </li>
            );
          })}
        </ul>
      )}
    </div>
  );
};

import React, { useState } from 'react';
import { experience } from './experience';

export const Expertise = ({ title }) => {
  return (
    <ul className={'space-y-4'}>
      {experience.map((item) => {
        return (
          <li
            key={item.date}
            className="grid gap-4 grid-cols-[100px_minmax(100px,_1fr)] md:grid-cols-[150px_minmax(500px,_1fr)]"
          >
            <div className="">
              <span className="font-bold">{item.info.company}</span>
              <div className="text-lightGreyForTime">{item.date}</div>
            </div>
            <div className="">
              <h2 className="font-bold">{item.info.job}</h2>
              <p>{item.info.description}</p>
            </div>
          </li>
        );
      })}
    </ul>
  );
};

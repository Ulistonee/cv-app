import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import { fetchSkills, selectSkills } from '../../features/skills/skillsSlice';
import { Loader } from '../Loader';
import { SkillForm } from './SkillForm';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPenToSquare } from '@fortawesome/free-solid-svg-icons';
import { Button } from '../Button';

export const Skills = () => {
  // const [isFormOpen, setIsFormOpen] = useState(false);
  const { skills, loading } = useSelector(selectSkills);
  const dispatch = useDispatch();

  useEffect(() => {
    // if (!localStorage.getItem('skills')) {
    //   dispatch(fetchSkills());
    // }
    dispatch(fetchSkills());
  }, []);

  // const postSkills = (data) =>
  //   fetch('/api/skills', {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/json'
  //     },
  //     body: JSON.stringify(data)
  //   }).then((res) => res.json());

  return (
    <div className={'w-full relative'}>
      {/*<Button*/}
      {/*  className={'px-7 absolute -top-10 right-0'}*/}
      {/*  variant={'secondary'}*/}
      {/*  data-testid={'open edit'}*/}
      {/*  onClick={() => setIsFormOpen((prevState) => !prevState)}*/}
      {/*>*/}
      {/*  <FontAwesomeIcon icon={faPenToSquare} className={'pr-2'} />*/}
      {/*  Open edit*/}
      {/*</Button>*/}
      {/*<div*/}
      {/*  className={clsx(*/}
      {/*    'overflow-hidden w-full transition-all duration-300 pt-5',*/}
      {/*    isFormOpen ? 'max-h-[500px]' : 'max-h-0'*/}
      {/*  )}*/}
      {/*>*/}
      {/*  <SkillForm onSubmitHandler={postSkills} />*/}
      {/*</div>*/}
      {loading && <Loader />}
      {!loading && skills.length > 0 && (
        <ul className={'mt-[40px] space-y-2 w-full'}>
          {skills.map((skill) => (
            <li
              key={skill.id}
              className={'bg-green px-4 py-1 text-white'}
              style={{ width: `${skill.range}%` }}
            >
              <div className={'truncate w-[calc(100%)]'}>{skill.name}</div>
            </li>
          ))}
        </ul>
      )}
      <div className={'mt-6 w-full flex justify-between border-t py-2 border-black relative'}>
        <div
          className={
            'text-sm text-left relative after:absolute after:w-full after:left-0 after:-top-2 after:h-[10px] after:-translate-y-full after:border-l after:border-black'
          }
        >
          Beginner
        </div>
        <div className={'absolute bg-white px-1 z-10 left-[20%] text-sm'}>
          <div
            className={
              'relative h-0 w-full after:absolute after:left-1/2 after:-top-2 after:-translate-y-full after:h-[10px] after:border-r after:border-black'
            }
          />
          Proficient
        </div>
        <div className={'absolute bg-white px-1 z-10 right-[20%] text-sm'}>
          <div
            className={
              'relative h-0 w-full after:absolute after:left-1/2 after:-top-2 after:-translate-y-full after:h-[10px] after:border-r after:border-black'
            }
          />
          Expert
        </div>
        <div
          className={
            'text-sm text-right relative after:absolute after:w-full after:left-0 after:-top-2 after:h-[10px] after:-translate-y-full after:border-r after:border-black'
          }
        >
          Master
        </div>
      </div>
    </div>
  );
};

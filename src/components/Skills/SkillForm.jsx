import React from 'react';
import { useForm } from 'react-hook-form';
import { clsx } from 'clsx';
import { useDispatch } from 'react-redux';
import { fetchSkills } from '../../features/skills/skillsSlice';
import { Button } from '../Button';

export const SkillForm = ({ onSubmitHandler }) => {
  const dispatch = useDispatch();
  const {
    reset,
    register,
    handleSubmit,
    formState: { isSubmitSuccessful, isValid, errors }
  } = useForm({
    mode: 'onChange'
  });
  const onSubmit = (data) => {
    onSubmitHandler(data).then(() => {
      dispatch(fetchSkills());
      reset();
    });
  };

  const isNumeric = (value) => {
    return !isNaN(parseFloat(value)) && isFinite(value);
  };

  return (
    <div className={'border border-green rounded-xl'}>
      <form
        data-testid={'submit-form'}
        onSubmit={handleSubmit(onSubmit)}
        className={'space-y-2 py-5 px-4'}
      >
        <div>
          <div>
            <label htmlFor="name" className={'mr-4 text-sm'}>
              Skill name:
            </label>
            <input
              placeholder={'Enter skill name'}
              type="text"
              name={'name'}
              className={clsx(
                'border rounded p-2 text-xs',
                errors.name ? 'border-error' : 'border-black'
              )}
              {...register('name', {
                required: 'Skill name is required field'
              })}
            />
          </div>
          <div
            className={clsx(
              'text-error mt-1 text-xs',
              errors?.name?.message ? 'visible' : 'invisible'
            )}
          >
            {errors?.name?.message || ''}
          </div>
        </div>
        <div>
          <div>
            <label htmlFor="range" className={'mr-4 text-sm'}>
              Skill range:
            </label>
            <input
              placeholder={'Enter skill range'}
              type="text"
              name={'range'}
              className={clsx(
                'border rounded p-2 text-xs',
                errors.range ? 'border-error' : 'border-black'
              )}
              {...register('range', {
                required: 'Skill range is required field',
                min: {
                  value: 10,
                  message: 'SkillRange must be greater then or equal to 10'
                },
                max: {
                  value: 100,
                  message: 'SkillRange must be less than or equal to 100'
                },
                validate: {
                  isNumeric: (value) => isNumeric(value) || 'Skill range must be a "number" type'
                }
              })}
            />
          </div>
          <div className={clsx('text-error mt-1 text-xs', errors?.range ? 'visible' : 'invisible')}>
            {errors?.range?.message || ''}
          </div>
        </div>
        <Button
          className={clsx('')}
          type={'submit'}
          disabled={!isValid}
          variant={isValid && isSubmitSuccessful ? 'secondary' : 'primary'}
        >
          Add skill
        </Button>
      </form>
    </div>
  );
};

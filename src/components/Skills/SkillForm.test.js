import React from 'react';
import { fireEvent, render, screen, act } from '@testing-library/react';
import { SkillForm } from './SkillForm';
import { Provider } from 'react-redux';
import store from '../../app/store';

test('SkillForm components renders without errors', () => {
  render(
    <Provider store={store}>
      <SkillForm />
    </Provider>
  );
});

test('Add skill is disabled when no input is given', async () => {
  const mockHander = jest.fn(() => {
    return Promise.resolve({
      json: () => Promise.resolve({ rates: { CAD: 1.42 } })
    });
  });
  render(
    <Provider store={store}>
      <SkillForm onSubmitHandler={mockHander} />
    </Provider>
  );
  const addSkillButton = screen.getByText('Add skill');
  await act(() => fireEvent.click(addSkillButton));

  expect(mockHander).toHaveBeenCalledTimes(0);
});

test('Add skill is enabled when valid input is given', async () => {
  const mockHander = jest.fn(() => {
    return Promise.resolve({
      json: () => Promise.resolve({ rates: { CAD: 1.42 } })
    });
  });
  const component = render(
    <Provider store={store}>
      <SkillForm onSubmitHandler={mockHander} />
    </Provider>
  );

  // Симуляция клика на кнопку
  const addSkillButton = screen.getByText('Add skill');
  const nameInput = component.container.querySelector('input[name="name"]');
  const rangeInput = component.container.querySelector('input[name="range"]');
  await act(() => fireEvent.change(nameInput, { target: { value: '10' } }));
  await act(() => fireEvent.change(rangeInput, { target: { value: '12' } }));
  await act(() => fireEvent.click(addSkillButton));

  // Проверка, что функция onClick вызывается
  expect(mockHander).toHaveBeenCalledTimes(1);
});

import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-scroll';
import {
  faUser,
  faGraduationCap,
  faPen,
  faGem,
  faSuitcase,
  faLocationArrow,
  faComment
} from '@fortawesome/free-solid-svg-icons';

const links = [
  {
    name: 'about me',
    icon: faUser,
    text: 'About me'
  },
  {
    name: 'education',
    icon: faGraduationCap,
    text: 'Education'
  },
  {
    name: 'experience',
    icon: faPen,
    text: 'Experience'
  },
  {
    name: 'skills',
    icon: faGem,
    text: 'Skills'
  },
  // {
  //   name: 'portfolio',
  //   icon: faSuitcase,
  //   text: 'Portfolio'
  // },
  {
    name: 'contacts',
    icon: faLocationArrow,
    text: 'Contacts'
  }
  // {
  //   name: 'feedbacks',
  //   icon: faComment,
  //   text: 'Feedbacks'
  // }
];

export const Navigation = ({ open }) => {
  return (
    <nav className="text-grey">
      <ul className={'flex flex-col gap-8 py-10'}>
        {links &&
          links.length > 0 &&
          links.map((link) => {
            return (
              <li className={'flex gap-4'} key={link.name}>
                <Link
                  smooth
                  spy={true}
                  to={link.name}
                  containerId={'container'}
                  duration={500}
                  className="hover:text-green hover:cursor-pointer whiteSpace-nowrap flex items-center px-4 gap-2"
                >
                  <div className="w-[40px] h-[16px] flex items-center justify-center">
                    <FontAwesomeIcon icon={link.icon} />
                  </div>
                  <div>{open && <span className={'hidden sm:inline-block'}>{link.text}</span>}</div>
                </Link>
              </li>
            );
          })}
      </ul>
    </nav>
  );
};

import React, { useEffect, useState } from 'react';
import { clsx } from 'clsx';
import { Box } from '../../components/Box';
import { Timeline } from '../../components/Timeline';
import { Expertise } from '../../components/Expertise';
import { Skills } from '../../components/Skills/Skills';
import { Portfolio } from '../../components/Portfolio';
import { Element } from 'react-scroll';
import { Address } from '../../components/Address';
import { Feedback } from '../../components/Feedback';
import { Navigation } from '../../components/Navigation';
import { Link } from 'react-scroll';
import { Link as RouterLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronUp } from '@fortawesome/free-solid-svg-icons';

const ToggleButton = ({ onClick }) => (
  <button
    data-testid={'burger'}
    className="flex flex-col justify-center w-[30px] h-[30px] mt-6 bg-dark rounded-r-md"
    onClick={onClick}
  >
    <div className="w-2/4 h-[2px] bg-white mt-1 ml-1"></div>
    <div className="w-2/4 h-[2px] bg-white mt-1 ml-1"></div>
    <div className="w-2/4 h-[2px] bg-white mt-1 ml-1 mb-1"></div>
  </button>
);

const Sidebar = ({ open, toggle }) => {
  return (
    <div className={clsx('flex items-start')}>
      <div
        data-testid={'sidebar'}
        className={clsx(
          'relative transition-all duration-300 ease-in overflow-hidden bg-dark min-h-full flex flex-col justify-between',
          open ? 'w-[74px] sm:w-[250px]' : 'w-0 sm:w-[54px]'
        )}
      >
        <div className="flex flex-col items-center justify-center pt-5 px-2">
          <img
            src="assets/images/avatar.png"
            alt="user avatar"
            className="pb-2 rounded-full w-32"
          />
          {open && (
            <p className="hidden sm:block text-white text-base font-bold">Aizhan Bexatova</p>
          )}
        </div>
        <div className={'absolute top-1/4'}>
          <Navigation open={open} />
        </div>
        <RouterLink
          to="/"
          className={clsx(
            'text-white text-sm mx-auto hover:cursor-pointer w-1/2 rounded flex justify-center items-center pb-5',
            open ? '' : 'bg-green'
          )}
        >
          <FontAwesomeIcon icon={faChevronUp} className={clsx('text-white mr-2 -rotate-90')} />
          <span data-testid={'back'}>{open ? 'Go back' : ''}</span>
        </RouterLink>
      </div>
      <ToggleButton onClick={toggle} />
    </div>
  );
};

export const CV = () => {
  const [sidebarOpen, setSidebarOpen] = useState(true);

  const toggleSidebar = () => {
    setSidebarOpen(!sidebarOpen);
  };

  return (
    <div className="flex relative">
      <Sidebar open={sidebarOpen} toggle={toggleSidebar}></Sidebar>
      <Link
        smooth
        spy={true}
        to="about me"
        containerId={'container'}
        duration={500}
        className="fixed bottom-2 right-5 p-2 rounded-t bg-black cursor-pointer"
      >
        <FontAwesomeIcon icon={faChevronUp} className="text-white" />
      </Link>

      <Element id="container" className="flex-1 overflow-y-auto max-h-screen pr-5 md:pr-10">
        <Element name="about me">
          <Box title="About me">
            I started my career as a developer in 2020. I worked as a developer in a technological
            giant Sber where I`ve grown as a specialist. Currently I am a freelancer and looking
            forward to work on different projects.
          </Box>
        </Element>
        <Element name="education">
          <Box title="Education">
            <Timeline />
          </Box>
        </Element>
        <Element name="experience">
          <Box title="Experience">
            <Expertise />
          </Box>
        </Element>
        <Element name="skills">
          <Box title={'Skills'}>
            <Skills />
          </Box>
        </Element>
        {/*<Element name="portfolio">*/}
        {/*  <Box title="Portfolio">*/}
        {/*    <Portfolio />*/}
        {/*  </Box>*/}
        {/*</Element>*/}
        <Element name="contacts">
          <Box title="Contacts">
            <Address />
          </Box>
        </Element>
        {/*<Element name={'feedbacks'}>*/}
        {/*  <Box title="Feedbacks">*/}
        {/*    <Feedback />*/}
        {/*  </Box>*/}
        {/*</Element>*/}
      </Element>
    </div>
  );
};

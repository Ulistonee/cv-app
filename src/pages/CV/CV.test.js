import { fireEvent, act, render, screen } from '@testing-library/react';
import { CV } from './CV';
import React from 'react';
import { Provider } from 'react-redux';
import store from '../../app/store';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Home } from '../Home';

test('CV component rendering', () => {
  render(
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path={'/'} element={<Home />} />
          <Route path="/CV" element={<CV />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
});

test('On click the menu toggles', async () => {
  const cv = render(
    <Provider store={store}>
      <BrowserRouter>
        <CV />
      </BrowserRouter>
    </Provider>
  );
  const back = cv.getByText('Go back');
  expect(back).toBeInTheDocument();
});

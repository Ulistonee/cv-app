import React from 'react';
import { render } from '@testing-library/react';
import { Home } from './Home';
import { Provider } from 'react-redux';
import store from '../../app/store';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { CV } from '../CV';

test('Home component rendering', () => {
  render(
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path={'/'} element={<Home />} />
          <Route path="/CV" element={<CV />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
});

import { createServer, Model, RestSerializer } from 'miragejs';
// const dbData = localStorage.getItem('db');

class Skills extends Model {}
Skills.displayName = {
  name: '',
  range: 10
};
export let server = createServer({
  models: {
    skills: Skills
  },

  serializers: {
    application: RestSerializer
  }
});
const defaultDB = {
  skills: [
    {
      name: 'Javascript',
      range: 80
    },
    {
      name: 'Typescript',
      range: 50
    },
    {
      name: 'HTML 5',
      range: 80
    },
    {
      name: 'CSS',
      range: 80
    },
    {
      name: 'Tailwind',
      range: 70
    },
    {
      name: 'React',
      range: 80
    }
  ],
  education: [
    {
      date: '2020',
      title: 'Ecole 42',
      description: 'Computer science'
    },
    {
      date: '2022 - 2023',
      title: 'EPAM Frontend School',
      description: 'Frontend development'
    }
  ]
};
server.db.loadData(defaultDB);

// server.pretender.handledRequest = function (verb) {
//   if (verb.toLowerCase() !== 'get' && verb.toLowerCase() !== 'head') {
//     localStorage.setItem('db', JSON.stringify(server.db.dump()));
//   }
// };

server.get('/api/education', (schema) => {
  return schema.db.education;
});

server.get('/api/skills', (schema) => {
  return schema.skills.all();
});
server.post('/api/skills', (schema, request) => {
  let attrs = JSON.parse(request.requestBody);
  return schema.skills.create(attrs);
});

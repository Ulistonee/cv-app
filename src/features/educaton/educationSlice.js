import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

export const initialState = {
  education: [],
  loading: false,
  error: ''
};

export const fetchEducation = createAsyncThunk('education/fetch', async () => {
  return fetch('/api/education', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((res) => res.json());
});

const educationReducer = createSlice({
  name: 'education',
  initialState,
  extraReducers: (builder) => {
    builder.addCase(fetchEducation.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchEducation.fulfilled, (state, action) => {
      state.loading = false;
      state.education = action.payload;
    });
    builder.addCase(fetchEducation.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });
  }
});

export default educationReducer.reducer;

export const selectEducation = (state) => state.education;
export const { ordered, restocked } = educationReducer.actions;
